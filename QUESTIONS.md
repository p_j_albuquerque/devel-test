## Referências Técnicas

1. Quais foram os últimos dois livros técnicos que você leu?
    1. PHP para quem conhece PHP (Juliano Niederauer)
    2. Desenvolvendo com Angular (Shyam Seshadri e Brad Green)

2. Quais foram os últimos dois framework/CMS que você trabalhou?
    1. CakePHP
    2. Symfony

3. Descreva os principais pontos positivos do seu framework favorito.
    1. Trabalha com AJAX de forma nativa
    2. Comunidade bastante ativa
    3. Leve
    4. Favilidade de trabalho com frameworks Front-End

4. Descreva os principais pontos negativos do seu framework favorito.
    1. Poucas classes de Helpers
    2. Atualização do famework requer testes no projeto
    3. Curva de aprendizado alta

5. O que é código de qualidade para você.
    É um código organizado, padronizado e documentado.

## Conhecimento Linux

1. O que é software livre?
    São os softwares quer qualquer pessoa pode fazer modificações no código sem pedir permissão ao seu proprietário.

2. Qual o seu sistema operacional favorito?
    Linux Mint

3. Já trabalhou com Linux ou outro Unix-like?
    Sim

4. O que é SSH?
    É uma forma de acessar um computador remoto de forma segura.

5. Quais as principais diferenças entre sistemas *nix e o Windows?
    1. Acesso total x Sem acesso
    2. Atualizações rápidas x Depende de fornecedor
    3. Maior segurança x Menor segurança contra ataques

## Conhecimento de desenvolvimento

1. O que é GIT?
    Sistema de Controle de Versão Distríbuido criado em 2005.

2. Descreva um workflow simples de trabalho utilizando GIT.
    1. O desenvolvedor utiliza apenas um branch (master).
    2. Ele clona o projeto para sua máquina
    3. Realiza alterações
    4. Faz o commit
    5. Realiza o pull para mesclar possíveis alterações no repositório central
    6. Realiza o push para enviar as alterações para o repositório central.

3. O que é PHP Data Objects?
    O PDO fornece uma camada de abstração em relação a conexão com o banco de dados.
    Ele efetua a conexão com diversos bancos de dados da mesma maneira, apenas alterando a string de conexão.

4. O que é Database Abstract Layer?
    É uma camada de abstração de banco de dados, que unifica a comunicação entre aplicação e o banco de dados.

5. Você sabe o que é Object Relational Mapping? Se sim, explique.
    Mapeia o banco de dados relacional em foma de objetos, transformando tabelas em Model e colunas em atributos.

6. Como você avalia seu grau de conhecimento em Orientação a objeto?
    Intermediário

7. O que é Dependency Injection?
    Conceito para reutilização, modularidade e testabilidade do código.
    Em vez de criar uma instância de um objeto, a classe ou função deverá solicitá-lo.

8. O significa a sigla S.O.L.I.D?
    1. S: Single Responsability Principle
    2. 0: Open/Close Principle
    3. L: Liskov Substitution Principle
    4. I: Interface Segregation Principle
    5. D: Dependency Inversion Principle

9. Qual a finalidade do framework PHPUnit?
    Realizar testes unitários a fim de facilitar o desenvolvimento e mostrar erros no determinado método.

10. Explique o que é MVC.
    1. Model local onde ficam os tratamentos de dados.
    2. View local onde são exibidas todas informações da aplicação para o usuário.
    3. Controller onde é controlada tod aplicação definindo se a requisição é para a VIEW ou para o MODEL para assim exibir informações corretas para o usuário.