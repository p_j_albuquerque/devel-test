<?php
    include "requerido.php";
    $index = new Index();
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Klick Site</title>
    <style>
        table{
            width: 100%;
        }
        td{
            border: 1px solid #000;
        }
    </style>
</head>
<body>
 <h1>Páginas</h1>
 <hr>
 <a href="add.php">Adicionar</a>
 <hr>
 <table>
    <tr>
        <th>ID</th>
        <th>Página</th>
        <th>Slug</th>
        <th>Author</th>
        <th>Ações</th>
    </tr>
    <?php while ($row = $index->pages->fetch_object()) { ?>
     <tr>
         <td><?php echo $row->id ?></td>
         <td><?php echo $row->title ?></td>
         <td><?php echo $row->slug ?></td>
         <td><?php echo $row->author ?></td>
         <td>
             <a href="edit.php?id=<?php echo $row->id ?>">Editar</a> |
             <a href="javascript:if (confirm('Deseja realmente apagar este conteúdo?')) {window.location = '?del=<?php echo $row->id ?>'}">Apagar</a>
         </td>
     </tr>
    <?php } ?>
 </table>

</body>
</html>
