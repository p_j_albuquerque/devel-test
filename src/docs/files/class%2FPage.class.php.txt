<?php
/**
* Criada para controlar a inserção e edição de páginas
* @author Paulo Albuquerque <p_j_albuquerque@hotmail.com>.
*/
class Page {

		/**
		* Variavel para receber mensagem de erro
		* @var String
		*/
		public $error;

		/**
		* Variavel para receber mensagem de sucesso
		* @var String
		*/
		public $success;

		/**
		* Variavel para receber os dados da pagina selecionada
		* @var String
		*/
		public $page;

		/**
        * Armazena a instancia do Objeto Pages
        * @var Object
        */
        protected $Pages;

	    /**
	    * Construtor criado para controlar o que será executado e pegar os dados da página atual
	    *
	    * @access public
        * @param String $page
	    */
		public function __construct($page){

			if($page == "add"){
				$this->add();
			}

			if($page == "edit"){
				$this->edit();
			}

			$this->get();
		}

		/**
        * Método para inserir pagina
        *
        * Caso o $_POST não esteja vazio, será feita a inserção de um registro no banco
        *
        * Caso o cadastro seja efetuado, será redirecionado para pagina de edição
        *
        * Caso o cadastro não seja efetuado, será exibido uma mensagem de erro
		* @access public
        */
		public function add(){
			$this->Pages = new Pages;
			if($_POST){
				$id = $this->Pages->insert($_POST);

				if($id){
					header("Location:edit.php?id=".$id);
				}else{
					$this->error = "Erro ao cadastrar tente novamente";
				}

			}

		}

		/**
		* Método para alterar pagina
		*
		* Caso o $_POST não esteja vazio, irá ser feita a alteração do registro
        *
        * Caso a alteração seja efetuada, será exibido uma mensagem de sucesso
        *
        * Caso a alteração não seja efetuada, será exibido uma mensagem de erro
		* @access public
        **/
		public function edit(){
			$this->Pages = new Pages;

			if($_POST){

				$return = $this->Pages->edit($_POST);

				if($return){
					$this->success = "Alteração efetuada com sucesso";
				}else{
					$this->error = "Erro ao editar tente novamente";
				}

			}

		}

		/**
		* Método para pegar os dados da pagina atual
		*
		* Caso o $_GET não esteja vazio e existir id, irá fazer um filter dos dados
		* @access public
        **/
		public function get(){
			$this->Pages = new Pages;
			if($_GET){
				extract($_GET);
				if($id){
					$filter['id'] = $id;
					$this->page = $this->Pages->get_list($filter)->fetch_object();
				}
			}
		}
}


 ?>

