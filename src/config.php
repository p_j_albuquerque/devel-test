<?php
	/**
	* Configurações iniciais da aplicação
	* @author Paulo Albuquerque <p_j_albuquerque@hotmail.com>.
	*/

	ini_set("display_errors", true);
	error_reporting(E_ALL);

	/**
	* Limpa o cache do navegador
	**/
	header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");

	/**
	* Seta a codificação e data do Brasil
	**/
	setlocale(LC_ALL, NULL);
	setlocale(LC_ALL, 'pt_BR');
	setlocale(LC_TIME, 'ptb', 'pt_BR', 'portuguese-brazil', 'bra', 'brazil', 'pt_BR.utf-8', 'pt_BR.iso-8859-1','br');
	date_default_timezone_set('America/Sao_Paulo');

    /**
    * Função para facilitar o debug de array
    * @param Array $array
    **/
	function pr( $array ) {
		echo '<pre>';
		print_r( $array );
		echo '</pre>';
	}

	/**
    * Constante para armazenar a URL do sistema
    **/
	define( 'SITE', 'http://' . $_SERVER['SERVER_NAME'] . '/src/' );

	/**
    * Constante para armazenar o diretório do sistema
    **/
	define( 'DIR_ROOT', dirname(__FILE__) . '/' );

	/**
    * Constante para armazenar o nome do banco de dados
    **/
	define( 'BD', 'test' );

	/**
    * Constante para armazenar o servidor do banco de dados
    **/
	define( 'SERVER', 'localhost' );

	/**
    * Constante para armazenar o usuario do  banco de dados
    **/
	define( 'USER', 'root' );

	/**
    * Constante para armazenar a senha do  banco de dados
    **/
	define( 'PASSWORD', '4321rewq' );

?>
