<?php
    include "requerido.php";
    $edit = new Page("edit");
 ?>
<html>
    <head>
        <title>Edit</title>
        <meta charset="UTF-8">
    </head>
    <body>
        <a href="<?php echo SITE ?>">Back</a>
        <hr>
        <h1>Editar Página</h1>
        <hr>
        <div class="error"><?php echo ($edit->error)?$edit->error:""; ?></div>
        <div class="success"><?php echo ($edit->success)?$edit->success:""; ?></div>
        <div>
            <form method="post">
                <table>
                    <tr>
                        <td>Title: * </td>
                        <td> <input type="text" maxlength="255" required name="title" value="<?php echo $edit->page->title ?>"></td>
                    </tr>
                    <tr>
                        <td>Slug: </td>
                        <td> <input type="text" maxlength="255" name="slug" value="<?php echo $edit->page->slug ?>"></td>
                    </tr>
                    <tr>
                        <td>Description: </td>
                        <td> <textarea name="description" rows="5" cols="50" ><?php echo $edit->page->description ?></textarea></td>
                    </tr>
                    <tr>
                        <td>Body: </td>
                        <td> <textarea name="body" rows="5" cols="50" ><?php echo $edit->page->body ?></textarea></td>
                    </tr>
                    <tr>
                        <td>Author: </td>
                        <td><input type="text" maxlength="255" name="author" value="<?php echo $edit->page->author ?>"></td>
                    </tr>
                    <tr style="text-align: right">
                        <td colspan="2">
                            <input type="hidden" name="id" value="<?php echo $edit->page->id ?>">
                            <input type="submit" value="Salvar">
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>