<?php
class FuncaoTest extends PHPUnit_Framework_TestCase
{

    protected $funcao;

    protected function setUp()
    {
        $this->funcao = new Funcao();
    }

    public function testRemoveSpecialCaracter()
    {
        $return = $this->funcao->remove_special_caracter("Revomento Caracteres Especiais %&!!");

        $this->assertEquals('revomento-caracteres-especiais', $return);
    }
}
?>