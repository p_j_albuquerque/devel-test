<?php
class PagesTest extends PHPUnit_Framework_TestCase
{

    protected $pages;
    protected $id;

    protected function setUp()
    {
        $this->pages = new Pages();
        $this->id = 1;
        if (!extension_loaded('mysqli')) {
            $this->markTestSkipped(
              'The MySQLi extension is not available.'
            );
        }
    }

    public function testLast()
    {
        $this->pages->setlast(1);

        $this->assertEquals(1, $this->pages->getlast());
    }

    public function testInsert()
    {
        $dados = array(
                'title' => 'Teste 1',
                'slug' => 'teste-1',
                'short_url' => 'AF8T3',
                'description' => 'Teste de descrição',
                'body' => 'Teste de conteudo',
                'author' => 'Paulo Albuquerque',
        );

        $dados['id'] = $this->id;

        $id = $this->pages->set($dados);

        $this->assertInternalType('integer', $id);
    }

    public function testEdit()
    {
        $filter['id'] = $this->id;

        $dados = array(
                'title' => 'Teste Alteração',
                'slug' => 'teste-alteracao',
                'short_url' => 'A68BT',
                'description' => 'Teste Descrição',
                'body' => 'Teste de conteudo',
                'author' => 'Paulo Albuquerque',
        );
        $return = $this->pages->update($dados, $filter);

        $this->assertTrue($return);
    }

    public function testDel()
    {
        $id = $this->id;
        $return = $this->pages->del($id);

        $this->assertTrue($return);
    }

    public function testShortURL()
    {
        $return = $this->pages->shortURL();

        $this->assertInternalType('string', $return);
    }

    public function testSlug()
    {
        $return = $this->pages->slug("Teste de Slug!!");

        $this->assertEquals('teste-de-slug', $return);
    }

}
?>