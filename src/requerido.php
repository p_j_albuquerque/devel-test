<?php
    require_once 'config.php';
    require_once DIR_ROOT .  'class/Mysql.class.php';

    /**
    * Inclue arquivo de classe assim que a mesma é instanciada
    * @param Class $class
    **/
    function autoload( $class) {
        require_once DIR_ROOT . 'class/'.$class.'.class.php';
    }
    spl_autoload_register("autoload");

?>
