<?php
$dados = array(
        'title' => $title,
        'slug' => $this->slug($title),
        'short_url' => $this->shortURL(),
        'description' => $description,
        'body' => $body,
        'author' => $author,
);
$id = $this->set($dados);
?>