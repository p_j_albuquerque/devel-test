<?php
$id = 5;
$filter['id'] = $id;

$dados = array(
        'title' => $title,
        'slug' => ($slug)?$this->slug($slug, $filter):$this->slug($title, $filter),
        'short_url' => $this->shortURL(),
        'description' => $description,
        'body' => $body,
        'author' => $author,
);
$return = $this->update($dados, $filter);
?>