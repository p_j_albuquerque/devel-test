CREATE TABLE `pages` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(255) NOT NULL,
    `slug` VARCHAR(255) NOT NULL,
    `description` VARCHAR(500) NOT NULL,
    `body` TEXT NOT NULL,
    `author` VARCHAR(200) NOT NULL,
    `insert_date` DATETIME NOT NULL,
    `update_date` DATETIME NOT NULL,
    PRIMARY KEY (`id`)
)
ENGINE=InnoDB;
