<?php
    include "requerido.php";
    $add = new Page("add");
 ?>
<html>
    <head>
        <title>Add</title>
        <meta charset="UTF-8">
    </head>
    <body>
        <a href="<?php echo SITE ?>">Back</a>
        <hr>
        <h1>Adicionar Página</h1>
        <hr>
        <div class="error"><?php echo ($add->error)?$add->error:""; ?></div>
        <div>
            <form method="post">
                <table>
                    <tr>
                        <td>Title: * </td>
                        <td> <input type="text" maxlength="255" required name="title" value=""></td>
                    </tr>
                    <tr>
                        <td>Description: </td>
                        <td> <textarea name="description" rows="5" cols="50" ></textarea></td>
                    </tr>
                    <tr>
                        <td>Body: </td>
                        <td> <textarea name="body" rows="5" cols="50" > </textarea></td>
                    </tr>
                    <tr>
                        <td>Author: </td>
                        <td><input type="text" maxlength="255" name="author" value=""></td>
                    </tr>
                    <tr style="text-align: right">
                        <td colspan="2"><input type="submit" value="Salvar"></td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>