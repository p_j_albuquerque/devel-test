<?php

    /**
    * Classe criada para fazer conexão com o banco de dados
    * @author Paulo Albuquerque <p_j_albuquerque@hotmail.com>.
    */
abstract class Mysql {

	/**
    * Atributo protegido para não permitir o uso nos objetos, mas sim nas classes filhas.
    * @var Object
    */
	static $connection;

	/**
    * Atributo protegido guarda o nome da tabela atual para ser utilizado nos metodos
    * @var String
    */
	protected $tableName;

    /**
    * Atributo para receber o mysql insert id.
    * @var Int
    */
	public $last;


    /**
    * Atributo para receber nome do banco de dados.
    * @var String
    */
	public $bd;

    /**
    * Atributo para receber ip do banco de dados.
    * @var String
    */
	public $server;

    /**
    * Atributo para receber usuário do banco de dados.
    * @var String
    */
	public $user;

    /**
    * Atributo para receber a senha do banco de dados.
    * @var String
    */
	public $password;

    /**
    * Método para atribuir valor ao $last
    * @param Int
    */
    public function setlast($last){
        $this->last = $last;
    }

    /**
    * Método para pegar valor do $last
    * @return Int
    */
    public function getlast(){
        return $this->last;
    }


    /**
    * Construtor para conectar ao banco de dados.
    */
    public function __construct() {
        if ( !self::$connection ) {
                $this->bd = BD;
                $this->server = SERVER;
                $this->user = USER;
                $this->password = PASSWORD;
            self::$connection = new mysqli($this->server,$this->user,$this->password,$this->bd);
        }
    }

    /**
    * Método para inserir registros
    * @param Array $data
    * @return Int
    * @example example/insert.php
    */
	public function set($data) {
        $data['insert_date'] = date('Y-m-d H:i:s');
		$query = "INSERT INTO " . $this->tableName . " (";
		foreach ( $data AS $i => $v ) {
			$col[] = "$i";
			$val[] = "'".$this->anti_injection($v)."'";
		}

		$query .= implode(",", $col) . ") VALUES (";
		$query .= implode(",", $val) . ")";

		$this->execSQL($query);
		return $this->last;

	}

    /**
    * Método para listar conteudo quando select simples
    * @param $filter Array
    * @param $order String
    * @param $limit String
    * @return Object
    * @example example/get_list.php
    */
    public function get_list($filter="",$order="", $limit="") {
        $query = "SELECT * FROM " . $this->tableName . " WHERE 1 ";

        $query .= $this->treat_filter($filter);

        if ( $order != "" ) {
            $query .= " ORDER BY ". $order;
        }

        if ( $limit != "" ) {
            $query .= " LIMIT ". $limit;
        }
        return $this->execSQL($query);

    }

    /**
    * Método para update registro
    * @param $data Array
    * @param $filter Array
    * @param $limit String
    * @return boolean
    * @example example/edit.php
    */
    public function update($data, $filter, $limit = NULL) {

        $data['update_date'] = date('Y-m-d H:i:s');

        if(empty($filter) || is_null($filter)){
            die("O parâmetro filter não foi definido!");
        }

        $query = "UPDATE " . $this->tableName . " SET ";
        foreach ($data as $i => $v) {
            $val[] = "$i='".$this->anti_injection($v)."'";
        }
        $query .= implode(",",$val) . " WHERE 1";


        $query .= $this->treat_filter($filter);

        if(!is_null($limit)){
            $query .= " LIMIT ".$limit;
        }
        $result = $this->execSQL($query);
        if ( $result ) {
            return true;
        }else{
            return false;
        }

    }

    /**
    * Metodo excluir registro
    * @param $filter Array
    * @return boolean
    * @example example/remove.php
    */
    public function remove($filter) {
        $query = "DELETE FROM " . $this->tableName . " WHERE 1";

        $query .= $this->treat_filter($filter);

        $result = $this->execSQL($query);
        if ( $result ) {
            return true;
        }else{
            return false;
        }
    }

    /**
    * Método para tratar filter através de array
    * @param $filter Array
    * @return String
    */
    public function treat_filter($filter){
        $query = "";
            /// Verifica se há filter
            if ($filter != "") {
                foreach ($filter as $c => $v) {

                    if (is_array($v)) {
                        $aux = array();
                        foreach ($v as $key => $value) {
                            if (is_array($value)) {
                                if(strtolower($value['comparador']) == "in" || strtolower($value['comparador']) == "not in") {
                                    $aux[] = $value['coluna'] ." " . $value['comparador']. " ".$value['valor']."";
                                } else {
                                    $comparador = ( $value['comparador'] ) ? $value['comparador'] : '=';
                                    $aux[] = $value['coluna'] ." " . $comparador . " '".$this->anti_injection($value['valor'])."'";
                                }
                            }else{
                                $relacao = ($key == "relacao")? $value : " AND ";
                            }
                        }
                        $filters[] = "(".implode(" " .$relacao." ", $aux).")";
                    }else{
                        $filters[] = " $c = '$v'";
                    }
                }

                $query .= " AND (" . implode(" AND ", $filters) . ")";
            }
            return $query;
    }


    /**
    * Método para tratar dados antes de inserir
    * @param $field String
    * @return String
    */
	public function anti_injection($field){
		if(get_magic_quotes_gpc()){
			$clean = self::$connection->real_escape_string(stripslashes($field));
		}else{
			$clean = self::$connection->real_escape_string($field);
		}
		return $clean;
	}


    /**
    * Método para executar função dentro do banco de dados
    * @param $sql String
    * @return Object
    */
	public function execSQL($sql){

		self::$connection->query("SET NAMES utf8");
		$result = self::$connection->query($sql);

		if($result){
			$this->setlast(self::$connection->insert_id);
			return $result;
		}else{
			echo self::$connection->error;
		}

	}

    /**
    * Método para destruir conexão após executada
    */
	public function __destruct() {
		if ( !self::$connection ) {
			self::$connection->close();
		}
	}
}
?>
