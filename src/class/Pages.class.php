<?php

    /**
    * Classe criada para controlar os dados sobre Pages
    * @author Paulo Albuquerque <p_j_albuquerque@hotmail.com>.
    */
class Pages extends Mysql {

	public $tableName = "pages";

    /**
    * Método para tratar dados antes de inserir pagina
    * @param Array $data
    * @access public
    * @return Int
    */
    public function insert($data){
        extract($data);

        $data = array(
                    'title' => $title,
                    'slug' => $this->slug($title),
                    'short_url' => $this->shortURL(),
                    'description' => $description,
                    'body' => $body,
                    'author' => $author,
            );
        $id = $this->set($data);
        return $id;
    }

    /**
    * Método para tratar dados antes de editar pagina
    * @param Array $data
    * @access public
    * @return boolean
    */
    public function edit($data){
        extract($data);
        $filter['id'] = $id;

        $data = array(
                    'title' => $title,
                    'slug' => ($slug)?$this->slug($slug, $filter):$this->slug($title, $filter),
                    'short_url' => $this->shortURL(),
                    'description' => $description,
                    'body' => $body,
                    'author' => $author,
            );
        $return = $this->update($data, $filter);
        return $return;
    }

    /**
    * Método para tratar dados andes de excluir pagina
    * @param Int $del
    * @access public
    */
    public function del($del){
        $filter['id'] = $del;
        return $this->remove($filter);
    }

    /**
    * Método para criar url unica de uma quantidade de caracter
    * @param Int $qtd
    * @access public
    */
    public function shortURL( $qtd = 5 ) {
        $chars = str_split( 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789' );

        do {
            shuffle( $chars );

            $start = rand( 0, ( count( $chars ) - $qtd ) );

            $array_code = array_slice( $chars, $start, $qtd );

            $code = implode( '', $array_code );

            $filter = array(
                    'short_url' => $code
                );

            $rs = $this->get_list( $filter );

        } while ( $rs->num_rows );

        return $code;
    }

    /**
    * Método para criar url unica através do titulo
    * @param Int $title
    * @param Array $filter
    * @access public
    */
    public function slug( $title, $filter = null ) {
        $funcao = new Funcao;
        $slug = $funcao->remove_special_caracter( strip_tags( $title ) );
        $id = $filter['id'];
        $filter = array(
                    array(
                        'relacao' => 'AND',
                        array(
                            'coluna' => 'id',
                            'valor' => $id,
                            'comparador' => '!='
                        )
                    ),
                    'slug' => $slug
                );

        $rs = $this->get_list( $filter );
        $numRow = $rs->num_rows;

        if ( $numRow > 0 ) {

            for ( $ctrl = 1; $ctrl <= $numRow; $ctrl++ ) {

                $filter = array(
                            array(
                                'relacao' => 'AND',
                                array(
                                    'coluna' => 'id',
                                    'valor' => $id,
                                    'comparador' => '!='
                                )
                            ),
                            'slug' => $slug . '-' . $ctrl
                        );

                $rs = $this->get_list( $filter );

                if ( $rs->num_rows == 0 ) {
                    $slug = $slug . '-' . $ctrl;
                } else {
                    $numRow++;
                }

            }
        }

        return $slug;
    }

}
?>
