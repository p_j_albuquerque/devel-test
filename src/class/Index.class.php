<?php

    /**
    * Classe criada para controlar a index
    * @author Paulo Albuquerque <p_j_albuquerque@hotmail.com>.
    */
    class Index {


        /**
        * Objeto com dados de todas as páginas
        * @var Object
        */
        public $pages;

        /**
        * Armazena a instancia do Objeto Pages
        * @var Object
        */
        protected $Pages;

        /**
        * Construtor criado para setar os dados a serem exibidos
        */
		public function __construct(){
			$this->Pages = new Pages;
            extract($_GET);

			$this->pages = $this->Pages->get_list();

             if(isset($del)){
                if($this->Pages->del($del))
                    header("Location:./");
            }

		}

	}

 ?>
